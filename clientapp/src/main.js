import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/store'
import vuetify from './plugins/vuetify'
import ApiService from '@/services/api.service'
import { ValidationProvider, extend } from 'vee-validate'
import { required } from 'vee-validate/dist/rules'

Vue.config.productionTip = false

extend('required', {
  ...required,
  message: 'The {_field_} field is required'
})

new Vue({
  router,
  store,
  vuetify,
  components: {
    ValidationProvider
  },
  beforeCreate () {
    ApiService.init(process.env.VUE_APP_ROOT_WEB_API)
  },
  render: h => h(App)
}).$mount('#app')
