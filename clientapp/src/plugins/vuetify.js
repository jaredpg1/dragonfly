import Vue from 'vue'
import Vuetify from 'vuetify/lib'

Vue.use(Vuetify)

export default new Vuetify({
  theme: {
    themes: {
      light: {
        primary: '#34657F',
        secondary: '#D69A2D',
        success: '#7C8034',
        error: '#AD6433',
        ibsGrey: '#757575',
        ibsLightGrey: '#EEEEEE',
        ibsBlue: '#34657F'
      }
    }
  }
})
