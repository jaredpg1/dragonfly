// import Vue from 'vue'
import ApiService from '@/services/api.service'
import DbService from '@/services/db.service'

const getters = {

  getEventList (state) {
    return state.eventList
  },
  getThumbSrc (state) {
    return state.thumbSrc
  },
  hasError (state) {
    return state.hasErrors
  },
  getErrorMessage (state) {
    return state.errorMessage
  },
  getMyEvents (state) {
    return state.myEvents
  }
}

const actions = {
  async refreshEventList ({ commit }) {
    // eslint-disable-next-line no-unused-vars
    const response = await ApiService.getAllEvents()
    if (response.status === 200) {
      if (Array.isArray(response.data)) {
        // good data
        await commit('setEventList', response.data)
        // save to indexDb
        DbService.addEventToDb(response.data)
      } else {
        // bad data, load cached data
        await commit('setHasError', true)
        await commit('setErrors', 'Not Array')
      }
    } else if (response === undefined) {
      // load cached data
      await commit('setHasError', true)
      await commit('setErrors', 'Undefined')
    } else {
      // error, load cached data
      await commit('setHasError', true)
      await commit('setErrors', response.statusText)
    }
  },
  async loadMoreEvents ({ commit }) {
    await commit('loadAdditionalEvents')
  },
  async checkIfComing ({ commit }, id) {
    const response = await ApiService.checkIfComing(id)
    return response.data.coming
  },
  async addEvent ({ commit }, attending) {
    const response = await ApiService.addEvent(attending)
    return response
  },
  async goGetImage ({ commit }, imageDto) {
    const response = await ApiService.getImage(imageDto)
    await commit('setImage', response)
  },
  async getCachedData ({ commit }) {
    const response = await DbService.getCachedData()
    await commit('setEventList', response)
  }
}

const mutations = {
  setEventList (state, events) {
    state.allEvents = events
    state.eventList = events.slice(0, 8)
  },
  loadAdditionalEvents (state) {
    const length = state.eventList.length
    state.eventList = state.allEvents.slice(0, length + 8)
  },
  setHasError (state, err) {
    state.hasErrors = err
  },
  setErrors (state, message) {
    state.errorMessage = message
  },
  setImage (state, image) {
    state.thumbSrc = image
  },
  setMyEvents (state, val) {
    state.myEvents = val
  }
}

const state = {
  eventList: [],
  thumbSrc: {},
  myEvents: false,
  hasErrors: false,
  errorMessage: ''
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
