import axios from 'axios'
// import axiosRetry from 'axios-retry'

const contentTypeKey = 'Content-Type'
const acceptKey = 'Accept'

const ApiService = {

  init (baseURL) {
    axios.defaults.baseURL = baseURL
    // this.applyHeaders()
  },

  applyHeaders (token) {
    axios.defaults.headers.common[contentTypeKey] = 'application/json'
    axios.defaults.headers.common[acceptKey] = 'application/json'
  },

  async getAllEvents () {
    const response = await axios.get('/events', {
      auth: {
        username: 'jaredtest',
        password: 'evalpass'
      }
    })
      .catch((ex) => {
        return ex
      })
    return response
  },
  async checkIfComing (id) {
    const response = await axios.get(`/events/${id}/status/jaredtest`, {
      auth: {
        username: 'jaredtest',
        password: 'evalpass'
      },
      headers: {
        'Content-Type': 'application/json'
      }
    })
    return response
  },
  // async addEvent (attending) {
  //   // let response = await this.saveUserAttending(attending)
  //   const response = await axios.put(`/events/${attending.id}/status/jaredtest`, {
  //     headers: {
  //       'Content-Type': 'application/json'
  //     },
  //     auth: {
  //       username: 'jaredtest',
  //       password: 'evalpass'
  //     },
  //     body: {
  //       'coming': attending.isAttending
  //     }
  //   })
  //   console.log(response)
  //   return response.ok
  // },
  async addEvent (attending) {
    let url = `http://dev.dragonflyathletics.com:1337/api/dfkey/events/${attending.id}/status/jaredtest`
    let username = 'jaredtest'
    let password = 'evalpass'

    let headers = new Headers()

    headers.append('Content-Type', 'application/json')
    headers.append('Authorization', 'Basic ' + btoa(username + ':' + password))
    const response = await fetch(url, {
      method: 'PUT',
      headers: headers,
      // auth: {
      //   username: 'jaredtest',
      //   password: 'evalpass'
      // },
      body: JSON.stringify({
        'coming': attending.isAttending
      })
    })
      .catch((ex) => {
        console.log('Error occured', ex)
      })
    return response
  },
  async getImage (imageDto) {
    const response = await axios.get(`events/${imageDto.eventId}/media/${imageDto.imageId}`, {
      auth: {
        username: 'jaredtest',
        password: 'evalpass'
      },
      headers: {
        'Content-Type': 'image/jpeg'
      }
    })
    return 'data:image/jpeg;' + btoa(unescape(encodeURIComponent(response.data)))
  }
}

export default ApiService
