import store from '@/store/store'
const DbService = {
  addEventToDb (event) {
    var DBOpenReq = window.indexedDB.open('Events', 1)
    DBOpenReq.onsuccess = (ev) => {
      let db = DBOpenReq.result
      var transaction = db.transaction('events', 'readwrite')
      transaction.oncomplete = (e) => {

      }
      var objectStore = transaction.objectStore('events')
      var objStoreReq = objectStore.add(event)
      objStoreReq.onsuccess = (success) => {
        console.log(success)
      }
    }
  },
  async getCachedData () {
    let data = []
    var DBOpenReq = window.indexedDB.open('Events', 1)
    DBOpenReq.onsuccess = async (ev) => {
      let db = DBOpenReq.result
      var transaction = db.transaction('events')
      var objectStore = transaction.objectStore('events')
      var keys = objectStore.getAllKeys()
      keys.onsuccess = (key) => {
        var request = objectStore.get(key.target.result.length)
        request.onsuccess = async (e) => {
          data = e.target.result
          store.commit('events/setEventList', data)
          store.commit('events/setHasError', false)
        }
      }
      // var transaction = db.transaction('events').objectStore('events').get(6).onsuccess = (e) => {
      //   // console.log('DB Get: ' + e.target.result)
      //   return e.target.result
      // }
    }
  }
}
export default DbService
