import Vue from 'vue'
import { ValidationProvider, ValidationObserver, extend, configure } from 'vee-validate'
import { required, email, confirmed } from 'vee-validate/dist/rules'

extend('required', {
  ...required,
  message: '{_field_} is required'
})
extend('email', email)
// extend('confirmed', confirmed)
extend('confirmed', {
  ...confirmed,
  message: '{_field_} must match'
})

configure({
  classes: {
    valid: 'is-valid', // one class
    invalid: 'is-outlined' // ['is-invalid', 'bad'] multiple classes
  }
})

Vue.component('ValidationProvider', ValidationProvider)
Vue.component('ValidationObserver', ValidationObserver)
